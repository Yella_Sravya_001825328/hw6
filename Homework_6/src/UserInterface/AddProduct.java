/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.*;
import com.sun.glass.events.KeyEvent;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class AddProduct extends javax.swing.JPanel {

    private JPanel mainProcessContainer;
    // private MarketList marketlist;
    private PersonList salesRepList;
    private ProductList productlist;
    private Business business;
    private Product product;
    private Supplier supplier;
    private Person person;
    private String suppName;
    private MarketList marketList;

    /**
     * Creates new form AddProduct
     */

    public AddProduct(JPanel mainProcessContainer, MarketList marketList, PersonList salesRepList, ProductList productlist, Business business, Product product, Supplier supplier, Person person, String suppName) {
        initComponents();
        this.mainProcessContainer = mainProcessContainer;
        this.salesRepList = salesRepList;
        this.productlist = productlist;
        this.business = business;
        this.product = product;
        this.supplier = supplier;
        this.suppName = suppName;
        this.marketList = marketList;
        for (Market market : marketList.getMarketList()) {
            marketeDrop.addItem(market.getMarket());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtFloorPrice = new javax.swing.JTextField();
        txtProductName = new javax.swing.JTextField();
        addProduct = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtActualPrice = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCeilingPrice = new javax.swing.JTextField();
        marketeDrop = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setBackground(new java.awt.Color(153, 255, 204));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(153, 255, 204));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel1.setText("Add Product");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 26, 198, 29));

        jLabel2.setText("Product Name:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 92, -1, -1));

        jLabel3.setText("Floor Price:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(29, 138, -1, -1));

        txtFloorPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFloorPriceKeyTyped(evt);
            }
        });
        jPanel1.add(txtFloorPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 135, 198, -1));

        txtProductName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProductNameKeyTyped(evt);
            }
        });
        jPanel1.add(txtProductName, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 89, 198, -1));

        addProduct.setText("ADD PRODUCT");
        addProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProductActionPerformed(evt);
            }
        });
        jPanel1.add(addProduct, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 293, 195, -1));

        backBtn.setText("<< Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        jPanel1.add(backBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 293, -1, -1));

        jLabel4.setText("Actual Price:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 185, -1, -1));

        txtActualPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtActualPriceKeyTyped(evt);
            }
        });
        jPanel1.add(txtActualPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 182, 198, -1));

        jLabel5.setText("Ceiling Price:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 233, -1, -1));

        txtCeilingPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCeilingPriceKeyTyped(evt);
            }
        });
        jPanel1.add(txtCeilingPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(118, 230, 198, -1));

        marketeDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--SELECT--" }));
        marketeDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                marketeDropActionPerformed(evt);
            }
        });
        jPanel1.add(marketeDrop, new org.netbeans.lib.awtextra.AbsoluteConstraints(549, 89, 110, -1));

        jLabel6.setText("Market Name");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(459, 92, -1, -1));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-1, 6, 680, 320));
    }// </editor-fold>//GEN-END:initComponents

    private void addProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addProductActionPerformed

        if (txtProductName.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter Product Name", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (txtFloorPrice.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter Floor Price", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (txtActualPrice.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter Actual Price", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (txtCeilingPrice.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter Ceiling Price", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (marketeDrop.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Please select a market", "Error", JOptionPane.ERROR_MESSAGE);
        } else {

            String productName = txtProductName.getText();
            int floorPrice = Integer.parseInt(txtFloorPrice.getText());
            int actualPrice = Integer.parseInt(txtActualPrice.getText());
            int ceilingPrice = Integer.parseInt(txtCeilingPrice.getText());
            Product p = null;
            p = productlist.addProduct();
            
            p.setProdName(productName);
            p.setFloorValue(floorPrice);
            p.setActualValue(actualPrice);
            p.setCeilValue(ceilingPrice);
            
            for(Market m : marketList.getMarketList()) {
                if(m.getMarket().equalsIgnoreCase(String.valueOf(marketeDrop.getSelectedItem()))) {
                    p.setMarket(m);
                    break;
                }
            }

            JOptionPane.showMessageDialog(null, "Product added successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
            txtActualPrice.setText("");
            txtCeilingPrice.setText("");
            txtFloorPrice.setText("");
            txtProductName.setText("");
        }
    }//GEN-LAST:event_addProductActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        mainProcessContainer.remove(this);
        Component[] componentArray = mainProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        SupplierMain manageS = (SupplierMain) component;
        manageS.refreshTable();
        CardLayout layout = (CardLayout) mainProcessContainer.getLayout();
        layout.previous(mainProcessContainer);

    }//GEN-LAST:event_backBtnActionPerformed

    private void txtProductNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProductNameKeyTyped
        char c = evt.getKeyChar();
        if (!(Character.isLetter(c) || (Character.isSpaceChar(c)) || c == KeyEvent.VK_DELETE)) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter letters only");
        }
    }//GEN-LAST:event_txtProductNameKeyTyped

    private void txtFloorPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFloorPriceKeyTyped
        char enter = evt.getKeyChar();
        if (!(Character.isDigit(enter))) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_txtFloorPriceKeyTyped

    private void txtActualPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtActualPriceKeyTyped
        char enter = evt.getKeyChar();
        if (!(Character.isDigit(enter))) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_txtActualPriceKeyTyped

    private void txtCeilingPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCeilingPriceKeyTyped
        char enter = evt.getKeyChar();
        if (!(Character.isDigit(enter))) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_txtCeilingPriceKeyTyped

    private void marketeDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_marketeDropActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_marketeDropActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addProduct;
    private javax.swing.JButton backBtn;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JComboBox<String> marketeDrop;
    private javax.swing.JTextField txtActualPrice;
    private javax.swing.JTextField txtCeilingPrice;
    private javax.swing.JTextField txtFloorPrice;
    private javax.swing.JTextField txtProductName;
    // End of variables declaration//GEN-END:variables
}
