/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.*;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gauri Y
 */
public class AddOrder extends javax.swing.JPanel {

    private JPanel mainProcessContainer;
    private Business business;
    private OrderList ordersList;
    private ProductList productList;
    private CustomerList customerList;

    /**
     * Creates new form AddOrder
     */
    public AddOrder(JPanel mainProcessContainer, Business business, OrderList ordersList, ProductList productList, Supplier supplier, CustomerList customerList) {
        initComponents();
        this.mainProcessContainer = mainProcessContainer;
        this.business = business;
        this.ordersList = ordersList;
        this.productList = productList;
        this.customerList = customerList;

        for (Product p : productList.getProductList()) {
            productDrop.addItem(p.getProdName());
        }

        for (Customer c : customerList.getCustomerList()) {
            customerDrop.addItem(c.getCustName());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        productDrop = new javax.swing.JComboBox<>();
        saleAmount = new javax.swing.JTextField();
        addOrder = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        customerDrop = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        saleQuantity = new javax.swing.JTextField();
        backBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(153, 255, 204));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("PRODUCT");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(181, 79, -1, -1));

        jLabel3.setText("SALE AMOUNT");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(159, 164, -1, -1));

        productDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECT --" }));
        productDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                productDropActionPerformed(evt);
            }
        });
        add(productDrop, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 76, 146, -1));

        saleAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                saleAmountKeyTyped(evt);
            }
        });
        add(saleAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 161, 146, -1));

        addOrder.setText("ADD ORDER");
        addOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addOrderActionPerformed(evt);
            }
        });
        add(addOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 265, 154, 33));

        jLabel4.setText("CUSTOMER");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(174, 117, -1, -1));

        customerDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECT --" }));
        customerDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerDropActionPerformed(evt);
            }
        });
        add(customerDrop, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 114, 146, -1));

        jLabel5.setText("QUANTITY");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(122, 202, -1, -1));

        saleQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saleQuantityActionPerformed(evt);
            }
        });
        saleQuantity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                saleQuantityKeyTyped(evt);
            }
        });
        add(saleQuantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 199, 146, -1));

        backBtn.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        backBtn.setText("<< BACK");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        add(backBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(122, 262, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel1.setText("Add Order");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 11, 139, 29));
    }// </editor-fold>//GEN-END:initComponents

    private void productDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_productDropActionPerformed
        // TODO add your handling code here:
        String p = (String) productDrop.getSelectedItem();
    }//GEN-LAST:event_productDropActionPerformed

    public Product getProductObject(String val) {
        for (Product p : productList.getProductList()) {
            if (p.getProdName().equalsIgnoreCase(val)) {
                return p;
            }
        }
        return null;
    }

    public Customer getCustomerObject(String val) {
        for (Customer c : customerList.getCustomerList()) {
            if (c.getCustName().equalsIgnoreCase(val)) {
                return c;
            }
        }
        return null;
    }

    private void addOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addOrderActionPerformed
        // TODO add your handling code here:
        int saleAmountVal = 0;
        int saleQuantityVal = 0;
        int selectedIndexProduct = productDrop.getSelectedIndex();
        int selectedIndexCustomer = customerDrop.getSelectedIndex();
        if(!saleAmount.getText().trim().equalsIgnoreCase("")) {
            saleAmountVal = Integer.parseInt(saleAmount.getText());
        }
        if(!saleQuantity.getText().trim().equalsIgnoreCase("")) {
            saleQuantityVal = Integer.parseInt(saleQuantity.getText());
        }

        if (saleAmountVal == 0) {
            JOptionPane.showMessageDialog(null, "Please enter sale amount", "Error", JOptionPane.ERROR_MESSAGE);
        }else if (saleQuantityVal == 0) {
            JOptionPane.showMessageDialog(null, "Please enter quantity of purchase", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (selectedIndexProduct == 0) {
            JOptionPane.showMessageDialog(null, "Please select a product", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (selectedIndexCustomer == 0) {
            JOptionPane.showMessageDialog(null, "Please select a customer", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String productName = (String) productDrop.getSelectedItem();
            String customerName = (String) customerDrop.getSelectedItem();

            Product p = getProductObject(productName);
            Customer c = getCustomerObject(customerName);

            Order order = new Order();
            order = ordersList.addOrder();
            order.setCustomer(c);
            order.setProduct(p);
            order.setOrderAmount(saleAmountVal);
            order.setOrderQuantity(saleQuantityVal);
            order.setSalesRepName(LoginMain.loggedUser);

            JOptionPane.showMessageDialog(null, "Order created successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_addOrderActionPerformed

    private void customerDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerDropActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerDropActionPerformed

    private void saleQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saleQuantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saleQuantityActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add yopur handling code here:
        mainProcessContainer.remove(this);
        CardLayout layout = (CardLayout) mainProcessContainer.getLayout();
        layout.previous(mainProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed

    private void saleAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saleAmountKeyTyped
       char enter = evt.getKeyChar();
        if(!(Character.isDigit(enter))){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_saleAmountKeyTyped

    private void saleQuantityKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saleQuantityKeyTyped
       char enter = evt.getKeyChar();
        if(!(Character.isDigit(enter))){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_saleQuantityKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addOrder;
    private javax.swing.JButton backBtn;
    private javax.swing.JComboBox<String> customerDrop;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JComboBox<String> productDrop;
    private javax.swing.JTextField saleAmount;
    private javax.swing.JTextField saleQuantity;
    // End of variables declaration//GEN-END:variables
}
