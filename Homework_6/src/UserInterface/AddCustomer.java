/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.*;
import com.sun.glass.events.KeyEvent;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gauri Y
 */
public class AddCustomer extends javax.swing.JPanel {

    private JPanel mainProcessContainer;
    private Business business;
    private CustomerList customerList;
    
    
    public AddCustomer(JPanel mainProcessContainer, Business business, CustomerList customerList) {
        initComponents();
        this.mainProcessContainer = mainProcessContainer;
        this.business = business;
        this.customerList = customerList;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        marketDrop = new javax.swing.JComboBox<>();
        custName = new javax.swing.JTextField();
        customerAge = new javax.swing.JTextField();
        genderDrop = new javax.swing.JComboBox<>();
        addCustomer = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(153, 255, 204));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Name");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(198, 93, -1, 26));

        jLabel2.setText("Age");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(206, 133, -1, -1));

        jLabel3.setText("Gender");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 164, -1, -1));

        jLabel4.setText("Market Type");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(165, 197, -1, -1));

        marketDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(marketDrop, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 197, 188, -1));

        custName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                custNameKeyTyped(evt);
            }
        });
        add(custName, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 99, 188, -1));

        customerAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                customerAgeKeyTyped(evt);
            }
        });
        add(customerAge, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 130, 188, -1));

        genderDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        add(genderDrop, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 161, 188, -1));

        addCustomer.setText("ADD CUSTOMER");
        addCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCustomerActionPerformed(evt);
            }
        });
        add(addCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 268, 188, -1));

        backBtn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        backBtn.setText("<< BACK");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        add(backBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(116, 261, -1, -1));

        jLabel5.setBackground(new java.awt.Color(0, 0, 204));
        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("Add Customer");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 11, 188, 37));
    }// </editor-fold>//GEN-END:initComponents

    private void addCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCustomerActionPerformed
        // TODO add your handling code here:
        String name = custName.getText().trim();
        String age = customerAge.getText().trim();
        String gender = genderDrop.getSelectedItem().toString();
        
        if(name.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter customer name", "Error", JOptionPane.ERROR_MESSAGE);
        } else if(age.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter customer age", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            Customer c = business.getCustomer();
            c = customerList.addCustomer();
            c.setCustName(name);
            c.setCustAge(age);
            c.setCustomerGender(gender);
            JOptionPane.showMessageDialog(null, "Customer added successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_addCustomerActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        mainProcessContainer.remove(this);
        CardLayout layout = (CardLayout) mainProcessContainer.getLayout();
        layout.previous(mainProcessContainer);
        
        
        
    }//GEN-LAST:event_backBtnActionPerformed

    private void custNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_custNameKeyTyped
    char c=evt.getKeyChar();
    if(!(Character.isLetter(c) ||  (Character.isSpaceChar(c))||  c==KeyEvent.VK_DELETE )){
        evt.consume();
        JOptionPane.showMessageDialog(null, "Enter letters only");
    }
    }//GEN-LAST:event_custNameKeyTyped

    private void customerAgeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerAgeKeyTyped
      char enter = evt.getKeyChar();
        if(!(Character.isDigit(enter))){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_customerAgeKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addCustomer;
    private javax.swing.JButton backBtn;
    private javax.swing.JTextField custName;
    private javax.swing.JTextField customerAge;
    private javax.swing.JComboBox<String> genderDrop;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JComboBox<String> marketDrop;
    // End of variables declaration//GEN-END:variables
}
