/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Revenue;

import java.util.Comparator;

/**
 *
 * @author Gauri Y
 */
public class CompareByMarket {
    public static Comparator<SalesPersonByMarket> marketComparator = new Comparator<SalesPersonByMarket>() {
        @Override
        public int compare(SalesPersonByMarket t, SalesPersonByMarket t1) {
            String market1 = t.getMarketName();
            String market2 = t1.getMarketName();

            return market1.compareTo(market2);
        }
    };
}
