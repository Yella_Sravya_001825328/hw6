/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Revenue;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class SalesPersonByMarketList {
    private ArrayList<SalesPersonByMarket> salesMarketList;
    
    public SalesPersonByMarketList() {
        salesMarketList = new ArrayList<SalesPersonByMarket>();
    }

    public ArrayList<SalesPersonByMarket> getSalesMarketList() {
        return salesMarketList;
    }

    public SalesPersonByMarket addData() {
        SalesPersonByMarket s = new SalesPersonByMarket();
        salesMarketList.add(s);
        return s;
    }
}
