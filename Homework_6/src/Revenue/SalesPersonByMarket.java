/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Revenue;

import Business.*;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author user
 */
public class SalesPersonByMarket {

    private int rowID;
    private String salesPersonName;
    private int salesPersonRevenue;
    private String marketName;
    private String productName;
    private int targetSaleValue;

    public int getTargetSaleValue() {
        return targetSaleValue;
    }

    public void setTargetSaleValue(int targetSaleValue) {
        this.targetSaleValue = targetSaleValue;
    }

    public int getRowID() {
        return rowID;
    }

    public void setRowID(int rowID) {
        this.rowID = rowID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public int getSalesPersonRevenue() {
        return salesPersonRevenue;
    }

    public void setSalesPersonRevenue(int salesPersonRevenue) {
        this.salesPersonRevenue = salesPersonRevenue;
    }

    @Override
    public String toString() {
        return String.valueOf(getRowID());
    }
}
