/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Gauri Y
 */
public class Customer {
    private String custName;
    private String custAge;
    private String customerGender;
    private String customerMarketType;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustAge() {
        return custAge;
    }

    public void setCustAge(String custAge) {
        this.custAge = custAge;
    }
    

    public String getCustomerMarketType() {
        return customerMarketType;
    }

    public void setCustomerMarketType(String marketType) {
        this.customerMarketType = marketType;
    }

    

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }
    
    @Override
    public String toString() {
        return getCustName();
    }
}
