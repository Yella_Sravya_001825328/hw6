/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Gauri Y
 */
public class Business {
    private Person salesRep;
    private Market market;
    private Customer customer;
    
    public Business() {
        salesRep = new Person();
        market = new Market();
        customer = new Customer();
    }

    public Person getSalesRep() {
        return salesRep;
    }

    public void setSalesRep(Person salesRep) {
        this.salesRep = salesRep;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    
}
