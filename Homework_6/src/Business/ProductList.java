/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class ProductList {
    private ArrayList<Product> productList;

    public ProductList() {
        productList = new ArrayList<>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }
    
    public Product addProduct() {
        Product p = new Product();
        productList.add(p);
        return p;
    }
    
    public void deleteProduct(Product p) {
        productList.remove(p);
    }
}
