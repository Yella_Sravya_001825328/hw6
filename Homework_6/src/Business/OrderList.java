/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class OrderList {
    private ArrayList<Order> orderList;
    
    public OrderList() {
        orderList = new ArrayList<Order>();
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }
    
    public Order addOrder() {
        Order o = new Order();
        orderList.add(o);
        return o;
    }
    
    public void deleteOrder(Order o) {
        orderList.remove(o);
    }
}
