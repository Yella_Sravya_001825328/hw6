/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Gauri Y
 */
public class PersonList {
    private static ArrayList<Person> salesRepList;

    public PersonList() {
        salesRepList = new ArrayList<Person>();
    }

    public ArrayList<Person> getSalesRepList() {
        return salesRepList;
    }
    
    public Person addSalesRep() {
        Person s = new Person();
        salesRepList.add(s);
        return s;
    }
    
    public void deleteSalesRep(Person s) {
        salesRepList.remove(s);
    }
    
    public Person isValidUser(String username, String password) {
        for(Person s : salesRepList) {
            if(s.getPersonUsername().equals(username) && s.getPersonPassword().equals(password)) {
                return s;
            }
        }
        return null;
    }
}
