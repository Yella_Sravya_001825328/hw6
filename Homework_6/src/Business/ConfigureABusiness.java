/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;



/**
 *
 * @author Gauri Y
 */
public class ConfigureABusiness {

    public static Business BusinessInitialize() {
        Business b = new Business();
        Person s = b.getSalesRep();

        PersonList person = new PersonList();
       
        s = person.addSalesRep();
        s.setPersonName("Gauri");
        s.setPersonUsername("gauri");
        s.setPersonRole("admin");
        try {
            s.setPersonPassword(PasswordEncrypt.PasswordEncrypt("gauri"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        s = person.addSalesRep();
        s.setPersonName("Sravya");
        s.setPersonUsername("sravya");
        s.setPersonRole("supplier");
        try {
            s.setPersonPassword(PasswordEncrypt.PasswordEncrypt("sravya"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        s = person.addSalesRep();
        s.setPersonName("Kandarp");
        s.setPersonUsername("kandarp");
        s.setPersonRole("sales rep");
        try {
            s.setPersonPassword(PasswordEncrypt.PasswordEncrypt("kandarp"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
        
    }
}
